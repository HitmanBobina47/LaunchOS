def make_int_list(d1, d2, value):
    retval = []
    for i in range(d1):
        retval += [[]]
        for _ in range(d2):
            retval[i] += [int(value)]
    return retval
