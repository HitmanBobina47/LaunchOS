from tkinter import *
import pluginbase

class Monitor(pluginbase.PluginBase):
    def __init__(self):
        self.root = None
        self.info = None
        self.label = None

    def activate(self):
        self.root = Tk()
        self.root.title('Debug Monitor')

        self.info = StringVar()
        self.label = Label(self.root, textvariable=self.info)
        self.label.pack()
        self.context.events.update += self.update

    def deactivate(self):
        self.root.quit()
        self.root = None
        self.info = None
        self.label = None
        self.context.events.update -= self.update

    def update(self):
        self.info.set(self.context.lc.display())
        self.root.update()
