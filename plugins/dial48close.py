import pluginbase

class Dial48Close(pluginbase.PluginBase):
    def __init__(self):
        self.done = 0

    def activate(self):
        self.context.lc.events.cc += self.shutdown

    def deactivate(self):
        self.context.lc.events.cc -= self.shutdown

    def shutdown(self, msg):
        if msg.control == 48:
            if self.done == 0 and msg.value == 127:
                self.done = 1
            elif self.done == 1 and msg.value == 0:
                self.context.events.exit()
            elif self.done == 1 and msg.value == 127:
                self.done = 0
