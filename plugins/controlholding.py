import pluginbase
from launch import LaunchControl

class ControlHolding(pluginbase.PluginBase):
    def __init(self):
        self.channel = 0

    def activate(self):
        self.context.lc.events.any_note += update
        for i in LaunchControl.button_ids:
            self.context.lc.send(Message('note_on', note=i, channel=self.channel, velocity=62))

    def deactivate(self):
        self.context.lc.events.any_note -= update

    def update(self, msg):
        if msg.channel == self.channel:
            up = 0
            for i in LaunchControl.button_ids:
                    if self.context.lc.notestates[self.channel][i] == 0:
                        up += 1
                        if up == len(LaunchControl.button_ids):
                            for j in LaunchControl.button_ids:
                                self.context.lc.send(Message('note_on', note=j, channel=self.channel, velocity=63))
                            break
                        self.context.lc.send(Message('note_on', note=i, channel=self.channel, velocity=15))
                    else:
                        self.context.lc.send(Message('note_on', note=i, channel=self.channel, velocity=60))
