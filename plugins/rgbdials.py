import pluginbase
from launch import LaunchControl

class RGBDials(pluginbase.PluginBase):
    def __init__(self):
        self.channel = 1
        self.red_dial = 8
        self.green_dial = 9
        self.blue_dial = 10
        self.button = 1
        self.auto_update = False

    def activate(self):
        self.context.events.update += self.update
        self.context.lc.events.on += self.pressed
        self.context.lc.events.hold1 += self.holding
        self.context.lc.events.cc += self.auto_RGB

    def deactivate(self):
        self.context.events.update -= self.update
        self.context.lc.events.on -= self.pressed
        self.context.lc.events.hold1 -= self.holding
        self.context.lc.events.cc -= self.auto_RGB

    def send_rgb(self):
        self.context.lp.send(self.context.lp.fullRGB(self.context.lc.dials[self.channel][self.red_dial]/2, self.context.lc.dials[self.channel][self.green_dial]/2, self.context.lc.dials[self.channel][self.blue_dial]/2))

    def update(self):
        if self.auto_update:
            self.context.lc.send(Message('note_on', channel=1, note=LaunchControl.button_ids[2], velocity=60))
            self.send_rgb()
        else:
            self.context.lc.send(Message('note_off', channel=1, note=LaunchControl.button_ids[2]))

    def holding(self, msg):
        if msg.channel == self.channel and msg.note == LaunchControl.button_ids[self.button]:
            self.auto_update = True

    def pressed(self, msg):
        if msg.type == 'note_on' and msg.channel == self.channel and msg.note == LaunchControl.button_ids[self.button]:
            self.auto_update = False
            self.send_rgb()
