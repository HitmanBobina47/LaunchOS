from events import Events
from mido import Message
from launch import MidiDevice, LaunchControl, LaunchpadMk2
from context import Context
from yapsy.PluginManager import PluginManager
import pluginbase, mido, time, jsonconfig, os

# Setup Context
c = Context(LaunchControl(), LaunchpadMk2(), PluginManager(plugin_info_ext='plugin'))

# Loads Plugins
c.pm.setPluginPlaces(['./plugins'])
c.pm.collectPlugins()
for pluginInfo in c.pm.getAllPlugins():
    pluginInfo.plugin_object.setup(c)
    pluginInfo.plugin_object.activate()

# Loads JSON Plugins
jsonconfig.setup(c)
for g in os.listdir('./plugins/json'):
    jsonconfig.load(f'./plugins/json/{g}')

# When the context's exit event is triggered, this makes the mainloop exit.
done = False
def exit():
    global done
    done = True
c.events.exit += exit

# Timing
eltime = time.time()
timestep = 1/30

while done == False:
    c.lc.poll()
    c.lp.poll()
    if time.time() > eltime+timestep:
        eltime = time.time()
        c.events.update()

for i in range(16):
    c.lc.reset()
