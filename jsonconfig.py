import json, pyautogui, subprocess, time

context = None

def setup(c):
    global context
    context = c

handlers = list()

def typewrite(string):
    pyautogui.typewrite(string)

def np_typewrite(string):
    p = subprocess.Popen(['notepad.exe'])
    time.sleep(0.1)
    pyautogui.typewrite(string)

def launcimg(lst):
    context.lp.send(context.lp.RGBs(lst))

class Handler():
    def __init__(self, device, msgtype, channel, handletype, handlearglst):
        self.device = device
        self.msgtype = msgtype
        self.channel = channel
        self.handletype = handletype
        self.handlearglst = handlearglst

    def handle(self, msg):
        if msg.type == self.msgtype and msg.channel == self.channel:
            if self.handletype == 'typewrite':
                typewrite(*self.handlearglst)
            elif self.handletype == 'np_typewrite':
                np_typewrite(*self.handlearglst)
            elif self.handletype == 'launchimg':
                launcimg(*self.handlearglst)

    def register(self):
        if self.device == 'launchcontrol':
            dev = context.lc
        elif self.device == 'launchpad':
            dev = context.lp

        if self.msgtype == 'note_on':
            dev.events.on += self.handle
        elif self.msgtype == 'note_off':
            dev.events.off += self.handle
        elif self.msgtype == 'control_change':
            dev.events.cc += self.handle

    def unregister(self):
        if self.device == 'launchcontrol':
            dev = context.lc
        elif self.device == 'launchpad':
            dev = context.lp

        if self.msgtype == 'note_on':
            dev.events.on -= self.handle
        elif self.msgtype == 'note_off':
            dev.events.off -= self.handle
        elif self.msgtype == 'control_change':
            dev.events.cc -= self.handle

class NoteHandler(Handler):
    def __init__(self, device, msgtype, channel, handletype, handlearglst, note):
        Handler.__init__(self, device, msgtype, channel, handletype, handlearglst)
        self.note = note

    def handle(self, msg):
        if msg.note == self.note:
            Handler.handle(self, msg)

class ControlHandler(Handler):
    def __init__(self, device, msgtype, channel, handletype, handlearglst, control, lowval, highval):
        Handler.__init__(self, device, msgtype, channel, handletype, handlearglst)
        self.control = control
        self.lowval = lowval
        self.highval = highval

    def handle(self, msg):
        if msg.control == self.control and msg.value <= self.highval and msg.value >= self.lowval:
            Handler.handle(self, msg)

def load(filename):
    global handlers
    lst = json.load(open(filename, 'r'))
    print(lst)
    for i in lst:
        if i['htype'] == 'note':
            handler = NoteHandler(i['device'], i['msgtype'], i['channel'], i['handletype'], i['handlearglst'], i['note'])
        elif i.htype == 'control':
            handler = ControlHandler(i['device'], i['msgtype'], i['channel'], i['handletype'], i['handlearglst'] or [], i['control'], i['lowval'] or 127, i['highval'] or 127)
        handler.register()
        handlers += [handler]
