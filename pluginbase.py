from context import Context
from yapsy.IPlugin import IPlugin

class PluginBase(IPlugin):
    def __init__(self):
        self.context = None

    def filter(self, msg):
        return True

    def onevent(self, msg):
        pass

    def setup(self, context):
        self.context = context

    def activate(self):
        pass

    def deactivate(self):
        pass

class ButtonHandler(PluginBase):
    def __init__(self):
        PluginBase.__init__(self)

class ExternalHandler(PluginBase):
    def __init__(self):
        PluginBase.__init__(self)
