import mido
done = 0
with mido.open_input('Launch Control 0') as port:
    while done < 128:
        message = port.receive()
        print(message)
        done += 1
