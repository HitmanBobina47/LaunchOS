import mido, time
from mido import Message
from events import Events
import util

class MidiDevice():
    def __init__(self, input='', output=''):
        self.input = input
        self.output = output
        self.wake()
        self.inp = mido.open_input(self.input)
        self.out = mido.open_output(self.output)
        self.notestates = util.make_int_list(16, 128, 0)
        self.dialstates = util.make_int_list(16, 128, 0)
        self.notes_held = list()
        self.events = Events(('on', 'off', 'cc', 'sysex', 'hold1', 'any', 'any_note'))

    def open(self):
        self.out = mido.open_output(self.output)
        self.inp = mido.open_input(self.input)

    def wake(self):
        temp = mido.open_output(self.output)
        time.sleep(0.01)
        temp.close()

    def close(self):
        self.inp.close()
        self.out.close()

    def hold_time(self, channel, note):
        if self.notestates[channel][note] == -1:
            return -1
        return time.time() - self.notestates[channel][note]

    def send(self, *args, **kwargs):
        return self.out.send(*args, **kwargs)

    def trigger(self, msg):
        for i in self.notes_held:
            if self.hold_time(i[0], i[1]) > 1 and i[2] < 1:
                self.events.hold1(Message('note_on', channel=i[0], note=i[1]))
                i[2] = 1
        if type(msg) == type(None):
            return False
        if msg.type == 'note_on':
            self.notestates[msg.channel][msg.note] = time.time()
            self.notes_held += [[msg.channel, msg.note, 0]]
            self.events.on(msg)
            self.events.any_note(msg)
        elif msg.type == 'note_off':
            self.notestates[msg.channel][msg.note] = 0
            for i in range(len(self.notes_held)):
                if self.notes_held[i][0] == msg.channel and self.notes_held[i][1] == msg.note:
                    self.notes_held.remove(self.notes_held[i])
                    break
            self.events.off(msg)
            self.events.any_note(msg)
        elif msg.type == 'control_change':
            self.events.cc(msg)
            self.dialstates[msg.channel][msg.control] = msg.value
        elif msg.type == 'sysex':
            self.events.sysex(msg)
        else:
            return msg
        self.events.any(msg)
        return msg

    def receive(self, *args, **kwargs):
        msg = self.inp.receive(*args, **kwargs)
        self.trigger(msg)

    def poll(self, *args, **kwargs):
        msg = self.inp.poll(*args, **kwargs)
        self.trigger(msg)

class LaunchControl(MidiDevice):
    button_ids = [9, 10, 11, 12, 25, 26, 27, 28, 114, 115, 116, 117]
    dial_ids = [21, 22, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45, 46, 47, 48]
    def __init__(self, input='search', output='search'):
        if input == 'search':
            names = mido.get_input_names()
            for name in names:
                if 'Launch Control' in name:
                    input = name
                    break
        if output == 'search':
            names = mido.get_output_names()
            for name in names:
                if 'Launch Control' in name:
                    output = name
                    break
        MidiDevice.__init__(self, input, output)
        self.events.on += self.set
        self.events.off += self.set
        self.events.cc += self.set
        self.events.sysex += self.set

    def set(self, msg):
        if msg.type == 'note_on':
            self.buttons[msg.channel][LaunchControl.button_ids.index(msg.note)] = 1
        elif msg.type == 'note_off':
            self.buttons[msg.channel][LaunchControl.button_ids.index(msg.note)] = 0
        elif msg.type == 'control_change' and msg.control > 100:
            if msg.value == 0:
                self.trigger(Message('note_off', channel=msg.channel, note=msg.control, velocity=msg.value))
            else:
                self.trigger(Message('note_on', channel=msg.channel, note=msg.control, velocity=msg.value))
        elif msg.type == 'control_change':
            self.dials[msg.channel][LaunchControl.dial_ids.index(msg.control)] = msg.value

    @property
    def buttons(self):
        retval = []
        for c in range(16):
            retval += [[]]
            for i in LaunchControl.button_ids:
                if self.notestates[c][i] == -1:
                    retval[c] += [0]
                else:
                    retval[c] += [1]
        return retval

    @property
    def holding(self):
        retval = []
        for c in range(16):
            retval += [[]]
            for i in LaunchControl.button_ids:
                retval[c] += [self.notestates[c][i]]
        return retval

    @property
    def dials(self):
        retval = []
        for c in range(16):
            retval += [[]]
            for i in LaunchControl.dial_ids:
                retval[c] += [self.dialstates[c][i]]
        return retval

    def reset(self):
        for i in range(16):
            self.send(Message('control_change', channel=i, control=0, value=0))

    def color(self, red, green):
        return 16*green + red + 12

    def display(self):
        string = ''
        string += 'Buttons' + '\n'
        for i in self.buttons:
            string += str(i) + '\n'
        string += '\n'
        string += 'Dials' + '\n'
        for i in self.dials:
            string += str(i) + '\n'
        string += '\n'
        string += 'Holding' + '\n'
        for i in self.notes_held:
            string += str(i) + '\n'
        string += '\n'
        return string

class LaunchpadMk2(MidiDevice):
    def __init__(self, input='search', output='search'):
        if input == 'search':
            names = mido.get_input_names()
            print('Input', names)
            for name in names:
                if 'Launchpad MK2' in name:
                    input = name
                    break
        if output == 'search':
            names = mido.get_output_names()
            print('Output', names)
            for name in names:
                if 'Launchpad MK2' in name:
                    output = name
                    break
        MidiDevice.__init__(self, input, output)

    def __set__(self, msg):
        pass

    def RGBs(self, colors):
        data = []
        for i in colors:
            data += i
        return Message('sysex', data=[0x00, 0x20, 0x29, 0x02, 0x18, 0x0B] + data)

    def RGB(self, note, red, green, blue):
        return self.RGBs([note, red, green, blue])

    def fullRGB(self, red, green, blue):
        data = []
        for y in range(8):
            for x in range(9):
                i = ((y + 1) * 10) + (x + 1)
                data += [[i, int(red), int(green), int(blue)]]
        for i in range(8):
            data += [[i+104, int(red), int(green), int(blue)]]
        data += [[111, int(red), int(green), int(blue)]]
        return self.RGBs(data)
