from events import Events

class Context():
    def __init__(self, lc, lp, pm):
        self.lc = lc
        self.lp = lp
        self.pm = pm
        self.events = Events(('exit', 'update'))
